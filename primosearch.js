/*
==========================================================
#  Primo Api Ajax 0.4
#  2011 Rauli Haverinen / The National Library of Finland
==========================================================

# ***** BEGIN LICENSE BLOCK *****
# Version: MPL 1.1/GPL 2.0
#
# The contents of this file are subject to the Mozilla Public License Version
# 1.1 (the "License"); you may not use this file except in compliance with
# the License. You may obtain a copy of the License at
# http://www.mozilla.org/MPL/
#
# Software distributed under the License is distributed on an "AS IS" basis,
# WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
# for the specific language governing rights and limitations under the
# License.
#
# The Original Code is the Primo Api Ajax program.
#
# The Initial Developer of the Original Code is
# Helsinki University Library, the National Library of Finland.
# Portions created by the Initial Developer are Copyright (C) 2011
# the Initial Developer. All Rights Reserved.
#
# Contributor(s):
#
# Alternatively, the contents of this file may be used under the terms of
# the GNU General Public License Version 2 or later (the "GPL"), in which
# case the provisions of the GPL are applicable instead
# of those above. If you wish to allow use of your version of this file only
# under the terms of either the GPL or the LGPL, and not to allow others to
# use your version of this file under the terms of the MPL, indicate your
# decision by deleting the provisions above and replace them with the notice
# and other provisions required by the GPL or the LGPL. If you do not delete
# the provisions above, a recipient may use your version of this file under
# the terms of any one of the MPL, the GPL or the LGPL.
#
# ***** END LICENSE BLOCK *****
*/


jQuery(document).ready(function() {
	
	// GENERAL AJAX SETUP
	jQuery.ajaxSetup({ 
		scriptCharset: "utf-8" , 
		contentType: "text/xml; charset=utf-8"
	});

});

function PrimoApiAjaxDoQuery(nativeurl,rootid,mode,query,items,view,requrl,imgsize,imgadj,imgurl) {

	// PROCESS VARIABLES FOR REQUEST
	
	if(imgsize) var imgsizes = imgsize.split("/");
	if(imgsizes) {
	// TODO change image sizes here
	// 1 set variables width height
	// 2 set css
	//drupal_add_css('div#PrimoApiAjax'.$i.' td.PrimoApiAjaxRecord div.PrimoApiAjaxRecordImage img { width: '.$imagewidth.'; height: '.$imageheight.'; } div#PrimoApiAjax'.$i.' td.PrimoApiAjaxRecord div.PrimoApiAjaxRecordImage { width: '.$imagewidth.'; height: '.$imageheight.'; } div#PrimoApiAjax'.$i.' td.PrimoApiAjaxRecord div.PrimoApiAjaxImageContainer { width: '.($imagewidth+20).'; }', array('group' => CSS_DEFAULT, 'every_page' => FALSE, 'type' => 'inline'));
	}
	if(imgadj) var imgadjs = imgadj.split("/");
//	if(imgurl) var imgurls = imgadj.split("/"); // TODO delimiter?
	

	var nativeRecord = nativeurl+"primo_library/libweb/action/display.do?";
	var apiurl = nativeurl+"primo_library/libweb/local_ui/API/";
	
	if(!requrl) {
		requrl = apiurl;
	}
	
	var paramstart = "&";

	if(!requrl.match(/\?/)) paramstart = "?";
	
	// request query
	
	if(query=="undefined"|| query=="null") query = "";

	if(query=="") {
	//	jQuery(rootelem).empty();
		jQuery(rootelem).append("<p><strong>No query term specified</strong></p>");
		return false;
	}
	
	query = escape(query);
	view = escape(view);

	var linkview = "";

	if (view != "undefined" && view != "null" && view != "" && view) linkview = "&amp;vid="+view;
	
	if(!items) items = 5;
	if(items>30) items = 30;
	
	// ROOT OBJECT TO APPEND HTML
	
	var rootelem = "#"+rootid;
	
	// SEARCH FORM SETUP
	
	var addform = false;
	
	if(mode=="search") {
		addform = true;
	}
	
	// REVIEW SETTINGS
	
	var getreviews = false;
	
	var querymode = mode;

	if(mode=="review") {
		var ritems = items;
		items = 1;
		querymode="search";
		getreviews = true;
	}
	
	// AJAX QUERY

	jQuery.ajax({
		type: "GET",
		dataType: "xml",
		url: requrl+paramstart+"mode="+querymode+"&query="+query+"&items="+items,
	//	context: document.body, 
      error: function(XMLHttpRequest, textStatus, errorThrown) {
			jQuery(rootelem).empty();
			jQuery(rootelem).append("<p>Loading content failed: "+textStatus+"</p>");
	//		addExternalSearchLink();
		},
		success: function(xml) {
			jQuery(rootelem).empty();
	//		jQuery(rootelem).append("<div id=\""+rootid+"_LoaderImage\" class=\"PrimoApiAjaxLoaderImage\"><img src=\"img/ajax-loader_app.gif\" alt=\"loading...\" /></div>"); // processing so fast, no need for application loader image


			// SETUP VARIABLES TO CREATE PAGE
			var html = "";

			var i = 0;
			var callback_gbs = new Array();
			var images_append = new Array();
			var javascript = "";
			
			// PARSE XML

			jQuery(xml).find("[nodeName=sear:DOC]").each(function() {

				if(jQuery(this).find("[nodeName=sear:FOLDER]").attr("folderName") == query || querymode == "search") {

					// SETUP RECORD VARIABLES
					
					var item_title = "";
					var item_title_short = "";
					var item_google_preview = "";
					var item_image = "";
					var html_images = "";
					var item_image_class = "";
					var item_creator = "";
					var item_creator_short = "";
					var item_year = "";
					var item_isbn = "";
					var item_oclcid = "";
					var item_lccn = "";
					var item_type = "";
					
					var item_image_script = "";

					// GET RECORD DATA
					
					jQuery(this).find("search").each(function() {
					
						// record title
						jQuery(this).find("title").each(function() {
							item_title_short = jQuery(this).text();
							item_title = item_title;
							if (item_title_short.length > 45) {
								item_title_short = item_title_short.substring(0,45)+"&hellip;";
							}
						});
						
						// record id
						jQuery(this).find("recordid").each(function() {
							item_id = jQuery(this).text();
						});
					});
					
					// record standard identifiers

					jQuery(this).find("addata").each(function() {
						jQuery(this).find("isbn").each(function() {
							if(jQuery(this).text() != "") {
								item_isbn = jQuery(this).text();
							}
						});
						jQuery(this).find("oclcid").each(function() {
							if(jQuery(this).text() != "") {
								item_oclcid = jQuery(this).text();
							}
						});
						jQuery(this).find("lccn").each(function() {
							if(jQuery(this).text() != "") {
								item_lccn = jQuery(this).text();
							}
						});
					});

					jQuery(this).find("display").each(function() {
						
						// record title if not yet set
						if(!item_title) {
							jQuery(this).find("title").each(function() {
								item_title = jQuery(this).text();
							});
						}
						
						// record author
						jQuery(this).find("creator").each(function() {
							item_creator = jQuery(this).text();
						});
						
						if(!item_creator) {
							jQuery(this).find("contributor").each(function() {
								item_creator = jQuery(this).text();
							});
						}
						item_creator_short = item_creator;
						if (item_creator_short.length > 30) {
							item_creator_short = item_creator_short.substring(0,30)+"&hellip;";
						}
						

						// record types for bulk images
						jQuery(this).find("type").each(function() {
							item_type = jQuery(this).text();
						});
						if (item_type=="") item_type = "default";

						jQuery(this).find("creationdate").each(function() {
							item_year = /\d{3,4}/.exec(jQuery(this).text());
						//	item_year = item_year.substring(0,4);
							if(item_year) {
								item_title += " ("+item_year+")";
								item_title_short += " ("+item_year+")";
							}
						});

					});
				
					// if real image available, if not, use default images
					jQuery(this).find("links").each(function() {
						jQuery(this).find("thumbnail").each(function() {
						
							// local images
							if(jQuery(this).text() != "" && !jQuery(this).text().match(/^\$\$T/)) {
								item_image = jQuery(this).text().replace(/^\$\$U/, "");
								item_image_class = "local";
							
							// images from external services
							// TODO set default image service if several available
							} else {
								if(item_isbn != "") {
									item_image = jQuery(this).text();
									
									// TODO amazon images
									if(item_image == "\$\$Tamazon_thumb") {
										item_image = "";//http://images.amazon.com/images/P/"+item_isbn+".01._SS[size]_SCLZZZZZZZ_.jpg";
										item_image_class = "amazon";
									
									// TODO syndetics images
									} else if(item_image == "\$\$Tsyndetics_thumb") {
										item_image = "";//http://syndetics.com/index.aspx?isbn="+item_isbn+"/SC.JPG&amp;client=CLIENT";
										item_image_class = "syndetics";
									
									// google books images
									} else if(item_image == "\$\$Tgoogle_thumb") {

										item_image ="";
										item_image_class = "google";
										
										callback_gbs.push("http://books.google.com/books?bibkeys=ISBN:"+item_isbn+",OCLC:"+item_oclcid+",LCCN:"+item_lccn+"&jscmd=viewapi&callback=PrimoApiAjaxUpdateGBSCover"+i);

										javascript += "<script>";
										javascript += "function PrimoApiAjaxUpdateGBSCover"+i+"(covers) {";
										javascript += "	var thumbUrl = PrimoApiAjaxGetGBSCover(covers);";
										javascript += "	var thumbImg = jQuery(\""+rootelem+"_RecordThumbnail_"+i+"\");";
										javascript += "	var previewUrl = PrimoApiAjaxGetGBSPreview(covers);";
										javascript += "	var previewLink = jQuery(\""+rootelem+"_RecordGooglePreviewLink_"+i+"\");";
										javascript += "	if (thumbImg && thumbUrl) {";
										javascript += "		thumbImg.attr(\"src\",thumbUrl.replace(\"zoom=5\",\"zoom=1\"));";
										javascript += "	} else {";
										javascript += "		jQuery(thumbImg).attr(\"src\",\""+apiurl+"img/noimage.png\");";
										javascript += "	}";
										javascript += "	if (previewLink && previewUrl) {";
										javascript += "		previewLink.attr(\"href\",previewUrl);";
										javascript += "	} else {";
										javascript += "		jQuery(previewLink).remove();";
										javascript += "	}";
										javascript += "}";
										javascript += "</script>";
							
									//	item_google_preview += "<a id=\""+rootid+"_RecordGooglePreviewLink_"+i+"\" href=\"\" class=\"PrimoApiAjaxRecordGooglePreview\" target=\"_blank\" title=\"Book preview at Google Book Search\"><img src=\""+apiurl+"img/google_preview.png\" alt=\"Google Preview\" /></a>";
										item_google_preview += "<a id=\""+rootid+"_RecordGooglePreviewLink_"+i+"\" href=\"\" class=\"PrimoApiAjaxRecordExternalLink\" target=\"_blank\" title=\"Book preview at Google Book Search\">Google Preview</a>";
				
									} else {
								//		alert(item_image);
									}
								}
							}
							
							// TODO when no image available
							/*
							if (item_image == "") {
								item_image = "img/noimage.png";
							}
							*/
							
							images_append.push(item_image);

						});
							html_images += "<img id=\""+rootid+"_RecordThumbnail_"+i+"\" src=\""+apiurl+"img/noimage.png\" class=\"PrimoApiAjax_image_"+item_image_class+"\" alt=\"\" />";
					});
					
					// combine record html
					
					if(item_creator)
						item_title = item_creator+" - "+item_title;

					html += "<td class=\"PrimoApiAjaxRecord PrimoApiAjaxRecord_"+i+"\"><div class=\"PrimoApiAjaxRecordContainer PrimoApiAjaxRecord_"+item_type+"\">";
					html += "<div class=\"PrimoApiAjaxRecordImage\"><a href=\""+nativeRecord+"doc="+item_id+linkview+"\" title=\""+item_title+"\" target=\"_blank\">"+html_images+"</a>"+item_google_preview+"</div>";
					html += "<div class=\"PrimoApiAjaxDetails\"><a href=\""+nativeRecord+"doc="+item_id+linkview+"\" title=\""+item_title+"\" target=\"_blank\"><span class=\"PrimoApiAjaxRecordCreator\">"+item_creator+"</span><span class=\"PrimoApiAjaxRecordTitle\">"+item_title_short+"</span></a></div>";
					html += "</div></td>\n";

					i++;
					if(i >= items) return false;
				}
			});

			// append html and additional javascript
			// TODO escaping? or does jquery enough?
			
			if(!html) html = "<div class=\"PrimoApiAjaxNotice\">No results!</div>";
			
			html = "<table class=\"PrimoApiAjaxResults\">"+"\n"+"<tr>"+"\n"+html+"</tr>"+"\n"+"</table>";
			
//			jQuery(rootelem).empty();
			jQuery(rootelem).addClass("PrimoApiAjax_"+mode);
			jQuery(rootelem).append(html);
			jQuery(rootelem).append(javascript);

	jQuery("div.PrimoApiAjax td.PrimoApiAjaxRecord").css("background-image","url('"+apiurl+"img/background_record.png')");
			// add images
			
			for(i=0;i<items;i++) {
				
				// request callback images from google books api
				if(callback_gbs[i]) {
					jQuery.ajax({
						type: "GET",
						url: callback_gbs[i],
						dataType: "script",
					/*	error: function() {
							jQuery(rootelem+"_RecordThumbnail_"+i).attr("src","img/noimage.png");
						}*/
					});
				}
				
				// replace local images
				var current = i-1;
				if(images_append[i]!="") {
					jQuery(rootelem+"_RecordThumbnail_"+current).attr("src",images_append[i]);
				}
				/*
				if(imgadjs[k]) {
				
				}
				*/
			}
			
			// get reviews
			if(getreviews==true && i>0) PrimoApiAjaxGetReviews(rootelem,query,ritems,requrl,paramstart,nativeRecord);
			
			// add search form
			if(addform==true) {
				var formquery = unescape(query);
				formquery = formquery.replace(/[<|>|"|']/ig,"");
		//		formquery = formquery.replace(/(<([^>]+)>)/ig,""); // remove tags (for input field)
				
				jQuery(rootelem).append("<form id=\""+rootid+"_Search\" class=\"PrimoApiAjax\" accept-charset=\"utf-8\" action=\"\"><fieldset><input id=\""+rootid+"_SearchQuery\" type=\"text\" value=\""+formquery+"\"/><input type=\"submit\" id=\""+rootid+"_SearchSubmit\" value=\"search\" /><div id=\""+rootid+"_SearchLink\" class=\"PrimoApiAjax_SearchLink\"><a href=\""+nativeurl+"primo_library/libweb/action/search.do?vl%28freeText0%29="+ formquery.replace(/(&)/ig, "") +"&fn=search\" target=\"_blank\">&gt;&gt;&gt; Toista haku KDK/Primossa</a></div></fieldset></form>");
				
				// TODO external search links value should be changed onclick too

				// handling form
//				jQuery(rootelem+"_SearchQuery").attr("value",query);
				jQuery(rootelem+"_Search").submit(function(){
					jQuery(rootelem).append("<div class=\"PrimoApiAjaxNotice\">Searching...</div>");
					query = jQuery(rootelem+"_SearchQuery").attr("value");
					PrimoApiAjaxDoQuery(nativeurl,rootid,querymode,query,items,view,requrl,imgsize,imgadj,imgurl);
					return false;
				});
/*				jQuery(rootelem+"_SearchSubmit").click(function() {
			//		jQuery(rootelem).css("height",jQuery(rootelem).height());
			//		jQuery(rootelem).empty();
					jQuery(rootelem).append("<div class=\"PrimoApiAjaxNotice\">Searching...</div>");
					query = jQuery(rootelem+"_SearchQuery").attr("value");
					PrimoApiAjaxDoQuery(rootid,mode,query,items,requrl,imgsize,imgadj,imgurl);
				});*/
/*				var key_code = null;
				jQuery(rootelem+"_SearchQuery").keypress(function(e) {
					key_code = (e.keyCode ? e.keyCode : e.which);
					query = jQuery(rootelem+"_SearchQuery").attr("value");
					if (key_code == 13)
						PrimoApiAjaxDoQuery(rootid,mode,query,items,requrl,imgsize,imgadj,imgurl);
				//	e.preventDefault();
				});*/
			}
      }
	});
	
}

function PrimoApiAjaxGetReviews(rootelem,query,ritems,requrl,paramstart,nativeRecord) {
	if(!ritems) ritems = 5;
	if(ritems>30) ritems = 30;
	jQuery.ajax({
		type: "GET",
		dataType: "xml",
		url: requrl+paramstart+"mode=review&query="+query+"&items="+items,
      error: function(XMLHttpRequest, textStatus, errorThrown) {
			jQuery(rootelem).append("<p>Loading reviews failed: "+textStatus+"</p>");
		},
		success: function(xml) {
			var reviews = new Array();
			var reviews_short = new Array();
			var reviews_html = "";
			// parse result
			jQuery(xml).find("Review").each(function() {
				jQuery(this).find("value").each(function() {
					var review = jQuery(this).text();
					var review_short = review;
				//	alert(jQuery(this).text());
					if (review_short.length > 100) {
						review_short = review_short.substring(0,100)+"&hellip;";
					}
					reviews_short.push(review_short); // add latest first .unshift() but the order is somewhat random??
					reviews.push(review); // add latest first .unshift()
				});
			});
			// collect and append html
			for (i = 0;i<ritems;i++) {
				if(reviews[i] && reviews_short[i])
					reviews_html += "<li title=\""+reviews[i]+"\">\""+reviews_short[i]+"\"</li>";
			}
			
	//		if(ritems<reviews.length)
				reviews_html += "<li><a href=\""+nativeRecord+"doc="+query+"&tabs=tagreviewsTab\" target=\"_blank\">[all reviews]</a></li>"; // "&vid="++
			
			jQuery(rootelem).append("<ul class=\"PrimoApiAjaxReviews\">"+reviews_html+"</ul>");
		}
	});
	
}

// general functions for google books api

function PrimoApiAjaxGetGBSCover(covers){
	for (i in covers) {
		var cover = covers[i];
		if (cover){
			return cover.thumbnail_url;
		}
	}
	return undefined;
}

function PrimoApiAjaxGetGBSPreview(covers){
	for (i in covers) {
		var cover = covers[i];
		if (cover){
			return cover.preview_url;
		}
	}
	return undefined;
}
