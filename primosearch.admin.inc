<?php

/**
* @file
* Administration page callbacks for primosearch module.
*/

function primosearch_admin_settings() {
  
  // NAVTIVE INTERFACE LOCATION
  $native_interface_location = 'http://kui-fe-01.csc.fi/';

  $form['primosearch_native'] = array(
    '#type' => 'textfield',
    '#title' => t('Primo location'),
    '#description' => t('Enter URL of Primo native interface.'),
    '#default_value' => variable_get('primosearch_native',$native_interface_location),
    '#size' => 100
  );
  
  // REQUEST SCRIPT LOCATION
  $request_script_location = 'http://kui-fe-01.csc.fi/primo_library/libweb/local_ui/API/';

  $form['primosearch_request_location'] = array(
    '#type' => 'textfield',
    '#title' => t('Primo API location'),
    '#description' => t('Enter URL of the Primo API.'),
    '#default_value' => variable_get('primosearch_request_location',$request_script_location),
    '#size' => 100
  );
  
  // REQUEST SCRIPT LOCATION
  $request_script_location = 'http://kui-fe-01.csc.fi/primo_library/libweb/local_ui/API/';

  $form['primosearch_default_view'] = array(
    '#type' => 'textfield',
    '#title' => t('Primo view for linking'),
    '#description' => t('Enter Primo view to link to.').' '.t('Leave empty for default').'.)',
    '#default_value' => variable_get('primosearch_default_view'),
    '#size' => 20
  );
  
  // JAVASCRIPT LOCATION
  
/*  $form['primosearch_js_external'] = array(
    '#type' => 'textfield',
    '#title' => t('External JavaScript'),
    '#description' => t('Location for the external JavaScript file.').$module_location,
    '#default_value' => variable_get('primosearch_js_external','http://kui-fe-01.csc.fi/primo_library/libweb/local_ui/API/PrimoApiAjax.js'),
    '#size' => 100
  );*/
  
  // CUSTOM CSS
  
/*  $form['primosearch_css_external'] = array(
    '#type' => 'textfield',
    '#title' => t('External CSS'),
    '#description' => t('Location for the external CSS file.'),
    '#default_value' => variable_get('primosearch_css_external','http://kui-fe-01.csc.fi/primo_library/libweb/local_ui/API/PrimoApiAjax.css'),
    '#size' => 100
  );*/
  
  $form['primosearch_css_imageheight'] = array(
    '#type' => 'textfield',
    '#title' => t('Image height'),
    '#description' => t('Customize image height.').' ('.t('Leave empty for default').' 150px.) '.t('Can be re-adjusted by <a href="@help">markup</a>.', array('@help' => url('admin/help/primosearch'))),
    '#default_value' => variable_get('primosearch_css_imageheight',''),
    '#size' => 5
  );
  
  $form['primosearch_css_imagewidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Image width'),
    '#description' => t('Customize image width.').' ('.t('Leave empty for default').' 100px.) '.t('Can be re-adjusted by <a href="@help">markup</a>.', array('@help' => url('admin/help/primosearch'))),
    '#default_value' => variable_get('primosearch_css_imagewidth',''),
    '#size' => 5
  );
  
  $form['primosearch_css_fontsize'] = array(
    '#type' => 'textfield',
    '#title' => t('Font size'),
    '#description' => t('Customize font size.').' ('.t('Leave empty for default').' 12px.)',
    '#default_value' => variable_get('primosearch_css_fontsize',''),
    '#size' => 4
  );
  
  $form['primosearch_css_custom'] = array(
    '#type' => 'textarea',
    '#title' => t('Custom CSS'),
    '#description' => t('Use inline CSS to customize appearance.'),
    '#default_value' => variable_get('primosearch_css_custom',''),
    '#cols' => 50,
    '#rows' => 10,
  );
  /*
  $form['primosearch_css_default'] = array(
    '#type' => 'checkbox',
    '#title' => t('Default CSS'),
    '#description' => t('Use default CSS (can be fine tuned above)'),
    '#default_value' => variable_get('primosearch_css_default',1),
  );*/
  
  
  // SUBMIT
  $form['#submit'][] = 'primosearch_admin_settings_submit';
  
  return system_settings_form($form);
}
function primosearch_admin_settings_submit() {
  //echo 'should do database update here...';
}
